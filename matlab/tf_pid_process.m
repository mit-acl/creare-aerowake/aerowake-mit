addpath(genpath('matlab_utilities/'));
set(0, 'DefaultFigureRenderer', 'painters');
close all

bagdir = '../bags/it_pt_2/';
bagname = 'it_cntrl_raw_3_PROCESSED';
bag = strcat(bagdir, bagname, '.bag');
t0 = 42;
tf = inf;

bagdata = processAllROSBagTopics(bag, false);

% command
com_t = bagdata.full_hl_command.t;
com_idx = get_indices_from_time(com_t, 20, t0, tf);
com_t = com_t(com_idx);
com_x = bagdata.full_hl_command.pose.position(1,com_idx);
com_y = bagdata.full_hl_command.pose.position(2,com_idx);
com_z = bagdata.full_hl_command.pose.position(3,com_idx);
com_u = bagdata.full_hl_command.twist.linear(1,com_idx);
com_v = bagdata.full_hl_command.twist.linear(2,com_idx);
com_w = bagdata.full_hl_command.twist.linear(3,com_idx);
com_phi = bagdata.full_hl_command.pose.orientation(1,com_idx); % controller places euler on quat data struct
com_tht = bagdata.full_hl_command.pose.orientation(2,com_idx);
com_psi = bagdata.full_hl_command.pose.orientation(3,com_idx);
com_p = bagdata.full_hl_command.twist.angular(1,com_idx);
com_q = bagdata.full_hl_command.twist.angular(2,com_idx);
com_r = bagdata.full_hl_command.twist.angular(3,com_idx);

% odometry
act_t = bagdata.indoor.odometry.t;
act_idx = get_indices_from_time(act_t, 20, t0, tf);
act_t = act_t(act_idx);
act_x = bagdata.indoor.odometry.pose.position(1,act_idx);
act_y = bagdata.indoor.odometry.pose.position(2,act_idx);
act_z = bagdata.indoor.odometry.pose.position(3,act_idx);
act_u = bagdata.indoor.odometry.twist.linear(1,act_idx);
act_v = bagdata.indoor.odometry.twist.linear(2,act_idx);
act_w = bagdata.indoor.odometry.twist.linear(3,act_idx);
act_phi = bagdata.indoor.odometry.pose.euler(1,act_idx);
act_tht = bagdata.indoor.odometry.pose.euler(2,act_idx);
act_psi = bagdata.indoor.odometry.pose.euler(3,act_idx);
act_p = bagdata.indoor.odometry.twist.angular(1,act_idx);
act_q = bagdata.indoor.odometry.twist.angular(2,act_idx);
act_r = bagdata.indoor.odometry.twist.angular(3,act_idx);

% calculate error
act_x_c = interp1(act_t, act_x, com_t);
act_y_c = interp1(act_t, act_y, com_t);
act_z_c = interp1(act_t, act_z, com_t);
act_u_c = interp1(act_t, act_u, com_t);
act_v_c = interp1(act_t, act_v, com_t);
act_w_c = interp1(act_t, act_w, com_t);
act_phi_c = interp1(act_t, act_phi, com_t);
act_tht_c = interp1(act_t, act_tht, com_t);
act_psi_c = interp1(act_t, act_psi, com_t);
act_p_c = interp1(act_t, act_p, com_t);
act_q_c = interp1(act_t, act_q, com_t);
act_r_c = interp1(act_t, act_r, com_t);
%
err_t = com_t;
err_x = com_x - act_x_c;
err_y = com_y - act_y_c;
err_z = com_z - act_z_c;
err_u = com_u - act_u_c;
err_v = com_v - act_v_c;
err_w = com_w - act_w_c;
err_phi = com_phi - act_phi_c;
err_tht = com_tht - act_tht_c;
err_psi = com_psi - act_psi_c;
err_p = com_p - act_p_c;
err_q = com_q - act_q_c;
err_r = com_r - act_r_c;

% extract pid terms
pid_t = bagdata.pid.t;
pid_idx = get_indices_from_time(pid_t, 1, t0, tf);
pid_t = pid_t(pid_idx);
pid_x_p = bagdata.pid.data(1,pid_idx);
pid_x_i = bagdata.pid.data(2,pid_idx);
pid_x_d = bagdata.pid.data(3,pid_idx);
pid_y_p = bagdata.pid.data(4,pid_idx);
pid_y_i = bagdata.pid.data(5,pid_idx);
pid_y_d = bagdata.pid.data(6,pid_idx);
pid_z_p = bagdata.pid.data(7,pid_idx);
pid_z_i = bagdata.pid.data(8,pid_idx);
pid_z_d = bagdata.pid.data(9,pid_idx);
pid_u_p = bagdata.pid.data(10,pid_idx);
pid_u_i = bagdata.pid.data(11,pid_idx);
pid_u_d = bagdata.pid.data(12,pid_idx);
pid_v_p = bagdata.pid.data(13,pid_idx);
pid_v_i = bagdata.pid.data(14,pid_idx);
pid_v_d = bagdata.pid.data(15,pid_idx);
pid_w_p = bagdata.pid.data(16,pid_idx);
pid_w_i = bagdata.pid.data(17,pid_idx);
pid_w_d = bagdata.pid.data(18,pid_idx);
pid_psi_p = bagdata.pid.data(19,pid_idx);
pid_psi_i = bagdata.pid.data(20,pid_idx);
pid_psi_d = bagdata.pid.data(21,pid_idx);

%% PLOTS

% x, y, z
figure('position', [10 10 3500 750])
subplot(1,3,1)
plot(err_t, err_x, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_x_p, 'r-', ...
     pid_t, pid_x_i, 'g-', ...
     pid_t, pid_x_d, 'b-')
hold off
xlabel('t (s)')
ylabel('x (m)')
legend('error', 'P', 'I', 'D')
subplot(1,3,2)
plot(err_t, err_y, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_y_p, 'r-', ...
     pid_t, pid_y_i, 'g-', ...
     pid_t, pid_y_d, 'b-')
hold off
xlabel('t (s)')
ylabel('y (m)')
legend('error', 'P', 'I', 'D')
subplot(1,3,3)
plot(err_t, err_z, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_z_p, 'r-', ...
     pid_t, pid_z_i, 'g-', ...
     pid_t, pid_z_d, 'b-')
hold off
xlabel('t (s)')
ylabel('z (m)')
legend('error', 'P', 'I', 'D')
saveas(gcf, strcat(bagdir, bagname, '-xyz.svg'))

% u, v, w
figure('position', [10 1100 3500 750])
subplot(1,3,1)
plot(err_t, err_u, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_u_p, 'r-', ...
     pid_t, pid_u_i, 'g-', ...
     pid_t, pid_u_d, 'b-')
hold off
xlabel('t (s)')
ylabel('u (m/s)')
legend('error', 'P', 'I', 'D')
subplot(1,3,2)
plot(err_t, err_v, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_v_p, 'r-', ...
     pid_t, pid_v_i, 'g-', ...
     pid_t, pid_v_d, 'b-')
hold off
xlabel('t (s)')
ylabel('v (m/s)')
legend('error', 'P', 'I', 'D')
subplot(1,3,3)
plot(err_t, err_w, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_w_p, 'r-', ...
     pid_t, pid_w_i, 'g-', ...
     pid_t, pid_w_d, 'b-')
hold off
xlabel('t (s)')
ylabel('w (m/s)')
legend('error', 'P', 'I', 'D')
saveas(gcf, strcat(bagdir, bagname, '-uvw.svg'))

% psi
figure('position', [2700 10 1000 750])
plot(err_t, err_psi, 'k-', 'linewidth', 2.5)
hold on; grid on
plot(pid_t, pid_psi_p, 'r-', ...
     pid_t, pid_psi_i, 'g-', ...
     pid_t, pid_psi_d, 'b-')
hold off
xlabel('t (s)')
ylabel('\psi (rad)')
legend('error', 'P', 'I', 'D')
saveas(gcf, strcat(bagdir, bagname, '-psi.svg'))