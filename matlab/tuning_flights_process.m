addpath(genpath('matlab_utilities/'));

bag   = '../bags/it_pt_2/indoor_tether_uav_2020-09-03-20-43-02.bag';
svfg1 = '../bags/it_pt_2/2_3-trk-pos.svg';
svfg2 = '../bags/it_pt_2/2_3-trk-ang.svg';

bagdata = processAllROSBagTopics(bag, false);
set(0, 'DefaultFigureRenderer', 'painters');

com_t = bagdata.full_hl_command.t;
com_idx = get_indices_from_time(com_t, 20, 20, inf);
com_t = com_t(com_idx);
com_x = bagdata.full_hl_command.pose.position(1,com_idx);
com_y = bagdata.full_hl_command.pose.position(2,com_idx);
com_z = bagdata.full_hl_command.pose.position(3,com_idx);
com_u = bagdata.full_hl_command.twist.linear(1,com_idx);
com_v = bagdata.full_hl_command.twist.linear(2,com_idx);
com_w = bagdata.full_hl_command.twist.linear(3,com_idx);
com_phi = 180/pi*bagdata.full_hl_command.pose.orientation(1,com_idx); % controller places euler on quat data struct
com_tht = 180/pi*bagdata.full_hl_command.pose.orientation(2,com_idx);
com_psi = 180/pi*bagdata.full_hl_command.pose.orientation(3,com_idx);
com_p = bagdata.full_hl_command.twist.angular(1,com_idx);
com_q = bagdata.full_hl_command.twist.angular(2,com_idx);
com_r = bagdata.full_hl_command.twist.angular(3,com_idx);

act_t = bagdata.odometry.t;
act_idx = get_indices_from_time(act_t, 20, 20, inf);
act_t = act_t(act_idx);
act_x = bagdata.odometry.pose.position(1,act_idx);
act_y = bagdata.odometry.pose.position(2,act_idx);
act_z = bagdata.odometry.pose.position(3,act_idx);
act_u = bagdata.odometry.twist.linear(1,act_idx);
act_v = bagdata.odometry.twist.linear(2,act_idx);
act_w = bagdata.odometry.twist.linear(3,act_idx);
act_phi = 180/pi*bagdata.odometry.pose.euler(1,act_idx);
act_tht = 180/pi*bagdata.odometry.pose.euler(2,act_idx);
act_psi = 180/pi*bagdata.odometry.pose.euler(3,act_idx);
act_p = bagdata.odometry.twist.angular(1,act_idx);
act_q = bagdata.odometry.twist.angular(2,act_idx);
act_r = bagdata.odometry.twist.angular(3,act_idx);

figure('position',[10 10 1200 500])
sgtitle('Translational Tracking')
subplot(2,3,1)
plot(com_t, com_x, 'r--', act_t, act_x, 'k-'); grid on
subplot(2,3,2)
plot(com_t, com_y, 'r--', act_t, act_y, 'k-'); grid on
subplot(2,3,3)
plot(com_t, com_z, 'r--', act_t, act_z, 'k-'); grid on
subplot(2,3,4)
plot(com_t, com_u, 'r--', act_t, act_u, 'k-'); grid on
subplot(2,3,5)
plot(com_t, com_v, 'r--', act_t, act_v, 'k-'); grid on
subplot(2,3,6)
plot(com_t, com_w, 'r--', act_t, act_w, 'k-'); grid on
saveas(gcf, svfg1)

figure('position',[10 10 1200 500])
sgtitle('Angular Tracking')
subplot(2,3,1)
plot(com_t, com_phi, 'r--', act_t, act_phi, 'k-'); grid on
subplot(2,3,2)
plot(com_t, com_tht, 'r--', act_t, act_tht, 'k-'); grid on
subplot(2,3,3)
plot(com_t, com_psi, 'r--', act_t, act_psi, 'k-'); grid on
subplot(2,3,4)
plot(com_t, com_p, 'r--', act_t, act_p, 'k-'); grid on
subplot(2,3,5)
plot(com_t, com_q, 'r--', act_t, act_q, 'k-'); grid on
subplot(2,3,6)
plot(com_t, com_r, 'r--', act_t, act_r, 'k-'); grid on
saveas(gcf, svfg2)