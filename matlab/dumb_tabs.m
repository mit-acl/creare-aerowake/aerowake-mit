function [] = dumb_tabs()

f = gcf;
graphics = f.Children.SelectedTab.Children;

myfig = figure('Position',[1000,1000,850,435]);
copyobj(graphics, myfig);
hgsave(myfig, 'tabsave.fig');

end