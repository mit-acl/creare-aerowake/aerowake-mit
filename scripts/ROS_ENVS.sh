##
## Functions which ALL COMPUTERS ON THE ROS NETWORK must call to participate
##

## SUPPORTING FUNCTIONS

# returns full path to scripts directory (will work IF no symlinks)
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# function for allowing odroid-in-the-loop (oil) IP ambiguity resolution
# for the ground station computer
function unambiguous_hostname() {
    HOSTNAME_I=$(hostname -I)
    for hn in $HOSTNAME_I; do
	if [[ $hn == 192.168.0.* ]]; then
          echo $hn
	fi
    done
}

# function for setting important ROS variables
function set_ros_vars() {
    export ROS_MASTER_URI="http://$1:11311"
    export ROS_IP=$(unambiguous_hostname)
    export ROS_HOSTNAME=$(unambiguous_hostname)
    env | grep ROS_MASTER_URI
    env | grep ROS_IP
    env | grep ROS_HOSTNAME
}

## ENVIRONMENTS

# For MIT's west high bay motion capture room
function ros_raven() {
  set_ros_vars 192.168.0.19;
  if [[ "$1" == "-t" ]]; then
    $SCRIPTDIR/raven_setup.sh
  fi
}

# west high bay motion capture room with odroid master
function ros_oraven() { set_ros_vars 192.168.0.140; }

# Make the local machine the ROS master
function ros_local() { set_ros_vars localhost; }

# oil environment with GROUND STATION ROS master
function ros_oil_g() { set_ros_vars 192.168.1.1; }

# oil environment with ODROID ROS master
function ros_oil_o() { set_ros_vars 192.168.1.140; }

# outdoor flight tests with creare platform and RAVEN_24_AEROWAKE network
function ros_aw() { set_ros_vars 192.168.0.148; }
